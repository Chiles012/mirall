FROM node:16 AS build-env

RUN mkdir /home/node/app
WORKDIR /home/node/app
COPY . .
RUN yarn
RUN yarn build

CMD ["yarn", "preview"]
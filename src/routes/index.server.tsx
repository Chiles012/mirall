import { Suspense } from 'react';
import type {Collection} from '@shopify/hydrogen/storefront-api-types';
const logo = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/MIRALL%20LOGO-05.png?alt=media&token=d0835c58-6ccc-4c9c-bd9a-0e21e3617bbf';
import { Image, Link } from '@shopify/hydrogen';
const showroom1 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/show1.jpeg?alt=media&token=a57af257-6e2a-4d7a-9faa-0321e05cac48'
const showroom2 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/show2.jpeg?alt=media&token=e468228b-c050-4d33-b016-f47d1ec7ebd0'
const img1 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/mesa.jpg?alt=media&token=e615a722-09bb-4ade-b7de-f1bfce4617cc'
const img2 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/lampara.jpg?alt=media&token=b71cd502-51ab-4904-b1ee-0c11ae62d94d'
const img3 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/espejo.jpg?alt=media&token=2f143a78-3408-4c3a-a5d1-6e3a665cd3bd'
const img4 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/30%20v2.jpg?alt=media&token=75d57dca-eb7f-4079-8ffa-2481d1d0671d'
const img5 = 'https://firebasestorage.googleapis.com/v0/b/mirallstudio.appspot.com/o/32%20v2.jpg?alt=media&token=704e899e-4cca-4b2d-b2a0-71dcfd7aec43'

import {
  CacheLong,
  gql,
  Seo,
  ShopifyAnalyticsConstants,
  useServerAnalytics,
  useLocalization,
  useShopQuery,
} from '@shopify/hydrogen';

import { MEDIA_FRAGMENT, PRODUCT_CARD_FRAGMENT } from '~/lib/fragments';
import { getHeroPlaceholder } from '~/lib/placeholders';
import { CartEmpty, FeaturedCollections, Grid, Hero } from '~/components';
import { CollectionCard, Layout, ProductSwimlane } from '~/components/index.server';
import {
  CollectionConnection,
  ProductConnection,
} from '@shopify/hydrogen/storefront-api-types';
import { TopProducts } from '~/components/cart/CartEmpty.client'
import {getImageLoadingPriority, PAGINATION_SIZE} from '~/lib/const';

export default function Homepage() {
  useServerAnalytics({
    shopify: {
      pageType: ShopifyAnalyticsConstants.pageType.home,
    },
  });
  // console.log(window.location.pathname);
  return (
    <Layout>
      <Suspense>
        <SeoForHomepage />
      </Suspense>
      <Suspense>
        <HomepageContent />
        <footer id='contacto' className="footer">
          <Image className='logofooter' src={logo} width={400} height={200} alt='logo' />
          <span className="tituloFooter">¿Quieres recibir descuentos especiales?</span>
          <span className="textoFooter">Suscríbete y se parte de nuestra comunidad.</span>
          <br />

          <div className="row">
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Correo electrónico" aria-label="Correo electrónico" aria-describedby="button-addon2" />
              <button className="btn btn-outline-secondary" type="button" id="button-addon2"><span className="material-symbols-outlined">
                arrow_right_alt
              </span></button>
            </div>
          </div>

        </footer>
      </Suspense>
    </Layout>
  );
}

function HomepageContent() {
  const {
    language: { isoCode: languageCode },
    country: { isoCode: countryCode },
  } = useLocalization();

  const {data: data2} = useShopQuery<any>({
    query: COLLECTIONS_QUERY,
    variables: {
      pageBy: PAGINATION_SIZE,
      country: countryCode,
      language: languageCode,
    },
    preload: true,
  });
  
  const collections: Collection[] = data2?.collections?.nodes;

  const { data } = useShopQuery<{
    heroBanners: CollectionConnection;
    featuredCollections: CollectionConnection;
    featuredProducts: ProductConnection;
  }>({
    query: HOMEPAGE_CONTENT_QUERY,
    variables: {
      language: languageCode,
      country: countryCode,
    },
    preload: true,
  });

  const { heroBanners, featuredCollections, featuredProducts } = data;

  // fill in the hero banners with placeholders if they're missing
  const [primaryHero, secondaryHero, tertiaryHero] = getHeroPlaceholder(
    heroBanners.nodes,
  );

  return (
    <>
      {/* {primaryHero && (
        <Hero {...primaryHero} height="full" top loading="eager" />
      )} */}

      {/* ------------------------------------------- */}
      <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
        <div className="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={img1} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={img2} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={img3} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={img4} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={img5} className="d-block w-100" alt="..." />
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Anterior</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Siguiente</span>
        </button>
      </div>

      <h2 className='titulo'>Más vendidos</h2>
      <div className="swimlane hiddenScroll md:pb-8 md:scroll-px-8 lg:scroll-px-12 md:px-8 lg:px-12">
        <TopProducts />
      </div>

      <div className="row showroom">
        <h2 className="col-12 col-md-8">
          Visita nuestro showroom en San Miguel de Allende y Puerto Vallarta
        </h2>
        <div className="col-12 col-md-4">
          <img className='imgShowroom' src={showroom1} alt="Showroom Miral" />
        </div>
        <div className="col-12 col-md-8">
          <img className='imgShowroom' src={showroom2} alt="Showroom Miral" />
        </div>
        <div className="col-12 col-md-4" style={{ padding: 25 }}>
          <a href='#contacto' className='btnShowroom'>Más información</a>
        </div>

      </div>
      <div style={{
        backgroundColor: '#103948',
        padding: '0 0 50px 0',
        marginTop: '50px'
      }}>
        <div className="row showroom" style={{ padding: 10 }}>
          <h2 style={{ textAlign: "center" }}>Conoce nuestras colecciones</h2>
          <Grid items={3}>
            {collections.map((collection, i) => {
              return (
                <>
                {
                  (i >= 3 && i <= 8 )  && (
                    <Link to={`/collections/${collection.handle}`} style={{ marginBottom: 25 }}>
                      <div className="card-image" style={{ display: "flex", flexDirection: "column", alignContent: "center" , alignItems: "center"}}>
                        {collection?.image && (
                          <Image
                            alt={`Image of ${collection.title}`}
                            data={collection.image}
                            height={200}
                            sizes="(max-width: 32em) 100vw, 33vw"
                            width={200}
                            widths={[400, 500, 600, 700, 800, 900]}
                            loaderOptions={{
                              scale: 2,
                              crop: 'center',
                            }}
                          />
                        )}
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            gap: 5,
                            marginTop: 10,
                            marginBottom: 10,
                          }}
                        >
                          <p style={{ color: "white", fontSize: 12, margin: 0 }}>{collection.title}</p>
                          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" style={{ fill: "rgba(255, 255, 255, 1)" }}><path d="m11.293 17.293 1.414 1.414L19.414 12l-6.707-6.707-1.414 1.414L15.586 11H6v2h9.586z"></path></svg>
                        </div>
                      </div>
                    </Link>
                  )
                }
                </>
              );
            })}
          </Grid>
        </div>

      </div>



      {/* <div className="colecciones">
        <h3>Conoce nuestras colecciones</h3>

        <a className='btnShowroom' href=""> Ver todo</a>
      </div> */}



      {/* ------------------------------------------- */}
      {/* <ProductSwimlane
        data={featuredProducts.nodes}
        title="Productos destacados"
        divider="bottom"
      />
      {secondaryHero && <Hero {...secondaryHero} />}
      <FeaturedCollections
        data={featuredCollections.nodes}
        title="Collections"
      /> */}
      {/* {tertiaryHero && <Hero {...tertiaryHero} />} */}
    </>
  );
}

function SeoForHomepage() {
  const {
    data: {
      shop: { title, description },
    },
  } = useShopQuery({
    query: HOMEPAGE_SEO_QUERY,
    cache: CacheLong(),
    preload: true,
  });

  return (
    <Seo
      type="homepage"
      data={{
        title,
        description,
        titleTemplate: '%s · Powered by Hydrogen',
      }}
    />
  );
}

const HOMEPAGE_CONTENT_QUERY = gql`
  ${MEDIA_FRAGMENT}
  ${PRODUCT_CARD_FRAGMENT}
  query homepage($country: CountryCode, $language: LanguageCode)
  @inContext(country: $country, language: $language) {
    heroBanners: collections(
      first: 3
      query: "collection_type:custom"
      sortKey: UPDATED_AT
    ) {
      nodes {
        id
        handle
        title
        descriptionHtml
        heading: metafield(namespace: "hero", key: "title") {
          value
        }
        byline: metafield(namespace: "hero", key: "byline") {
          value
        }
        cta: metafield(namespace: "hero", key: "cta") {
          value
        }
        spread: metafield(namespace: "hero", key: "spread") {
          reference {
            ...Media
          }
        }
        spreadSecondary: metafield(namespace: "hero", key: "spread_secondary") {
          reference {
            ...Media
          }
        }
      }
    }
    featuredCollections: collections(
      first: 3
      query: "collection_type:smart"
      sortKey: UPDATED_AT
    ) {
      nodes {
        id
        title
        handle
        image {
          altText
          width
          height
          url
        }
      }
    }
    featuredProducts: products(first: 12) {
      nodes {
        ...ProductCard
      }
    }
  }
`;

const COLLECTIONS_QUERY = gql`
  query Collections(
    $country: CountryCode
    $language: LanguageCode
    $pageBy: Int!
  ) @inContext(country: $country, language: $language) {
    collections(first: $pageBy) {
      nodes {
        id
        title
        description
        handle
        seo {
          description
          title
        }
        image {
          id
          url
          width
          height
          altText
        }
      }
    }
  }
`;

const HOMEPAGE_SEO_QUERY = gql`
  query homeShopInfo {
    shop {
      title: name
      description
    }
  }
`;

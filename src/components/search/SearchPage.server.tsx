import { Heading, Input, PageHeader } from '~/components';
import { Layout } from '~/components/index.server';
import { IconSearch } from '~/components';

export function SearchPage({
  searchTerm,
  children,
}: {
  searchTerm?: string | null;
  children: React.ReactNode;
}) {
  return (
    <Layout>
      <PageHeader className='search'>
        <Heading as="h1" size="copy">
          Buscar
        </Heading>
        <form className="relative flex w-full text-heading">
          <Input
            defaultValue={searchTerm}
            placeholder="Buscar..."
            type="search"
            variant="search"
            name="q"
          />
          <button className="absolute right-0 py-2" type="submit">
            <IconSearch className='iconSearch' />
          </button>
        </form>
      </PageHeader>
      <div className="txtVerde">
        {children}
      </div>
    </Layout>
  );
}

import { useEffect, useCallback, useState } from 'react';

import {
  useProductOptions,
  isBrowser,
  useUrl,
  AddToCartButton,
  Money,
  OptionWithValues,
  ShopPayButton,
} from '@shopify/hydrogen';

import { Heading, Text, Button, ProductOptions } from '~/components';
import { Toaster, toast } from 'react-hot-toast';

export function ProductForm() {
  const { pathname, search } = useUrl();
  const [params, setParams] = useState(new URLSearchParams(search));

  const { options, setSelectedOption, selectedOptions, selectedVariant } =
    useProductOptions();

  const isOutOfStock = !selectedVariant?.availableForSale || false;
  const isOnSale =
    selectedVariant?.priceV2?.amount <
    selectedVariant?.compareAtPriceV2?.amount || false;

  useEffect(() => {
    if (params || !search) return;
    setParams(new URLSearchParams(search));
  }, [params, search]);

  useEffect(() => {
    (options as OptionWithValues[]).map(({ name, values }) => {
      if (!params) return;
      const currentValue = params.get(name.toLowerCase()) || null;
      if (currentValue) {
        const matchedValue = values.filter(
          (value) => encodeURIComponent(value.toLowerCase()) === currentValue,
        );
        setSelectedOption(name, matchedValue[0]);
      } else {
        params.set(
          encodeURIComponent(name.toLowerCase()),
          encodeURIComponent(selectedOptions![name]!.toLowerCase()),
        ),
          window.history.replaceState(
            null,
            '',
            `${pathname}?${params.toString()}`,
          );
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = useCallback(
    (name: string, value: string) => {
      setSelectedOption(name, value);
      if (!params) return;
      params.set(
        encodeURIComponent(name.toLowerCase()),
        encodeURIComponent(value.toLowerCase()),
      );
      if (isBrowser()) {
        window.history.replaceState(
          null,
          '',
          `${pathname}?${params.toString()}`,
        );
      }
    },
    [setSelectedOption, params, pathname],
  );

  var nativeShare = function () {
    if (navigator.share) {
      navigator.share({ title: "titulo", text: "texto", url: "URL" })
    }
    return false;
  }

  let precioElevado = Number(selectedVariant.priceV2!.amount) * .2
  precioElevado = precioElevado + Number(selectedVariant.priceV2!.amount)

  const dollarprice = Intl.NumberFormat('en-US')

  return (
    <form className="grid gap-2">
      
      <div 
        style={{
          display: 'flex',
          gap: '10px',
        }}
      >
        <span className='precioElevado'>${dollarprice.format(precioElevado)}.00 MXN</span>
        <p><Money
          withoutTrailingZeros
          data={selectedVariant.priceV2!}
          as="span"
        />.00 MXN</p>
      </div>
      <p style={{ margin: 0 }}>O 12 MSI de ${dollarprice.format(Number(selectedVariant.priceV2?.amount)/12)}</p>

      <p style={{ margin: 0 }}>Los gastos de envío se calculan en la pantalla de pago</p>
      <p style={{ margin: 0 }}>ENVÍO GRATIS A TODA LA REPÚBLICA</p>
      <p style={{ margin: 0 }}>+ 20% de Descuento en pago de contado</p>
      
      {
        <div className="grid gap-4">
          {(options as OptionWithValues[]).map(({ name, values }) => {
            if (values.length === 1) {
              return null;
            }
            return (
              <div
                key={name}
                className="flex flex-col flex-wrap mb-4 gap-y-2 last:mb-0"
              >
                <Heading as="legend" size="lead" className="min-w-[4rem]">
                  {name}
                </Heading>
                <div className="flex flex-wrap items-baseline gap-4">
                  <ProductOptions
                    name={name}
                    handleChange={handleChange}
                    values={values}
                  />
                </div>
              </div>
            );
          })}
        </div>
      }
      <div className="grid items-stretch">
        <AddToCartButton
          variantId={selectedVariant?.id}
          quantity={1}
          accessibleAddingToCartLabel="Adding item to your cart"
          disabled={isOutOfStock}
          type="button"
          onClick={() => {
            toast.success('Producto agregado al carrito');
          }}
        >
          <Toaster
              position="right"
              reverseOrder={false}
          />
          <Button
            className='btnBlanco btnApp'
            width="full"
            as="span"
          >
            {isOutOfStock ? (
              <Text className='btnApp'>Agotados</Text>
            ) : (
              <Text
                as="span"
                className=" justify-center gap-2"
              >
                <span>Agregar al carrito</span> <span>·</span>{' '}
                <Money
                  withoutTrailingZeros
                  data={selectedVariant.priceV2!}
                  as="span"
                />.00 MXN
                {isOnSale && (
                  <Money
                    withoutTrailingZeros
                    data={selectedVariant.compareAtPriceV2!}
                    as="span"
                    className="opacity-50 strike"
                  />
                )}
              </Text>
            )}
          </Button>
        </AddToCartButton>
        {/* {!isOutOfStock && <ShopPayButton className='btnApp' variantIds={[selectedVariant.id!]} />} */}
        <button className='btn' onClick={nativeShare}><span class="material-symbols-outlined">
          share
        </span> Compartir</button>
      </div>
    </form>
  );
}

import React, {useRef} from 'react';
import {useScroll} from 'react-use';
import {
  Link,
  useCart,
  CartLineProvider,
  CartShopPayButton,
  Money,
} from '@shopify/hydrogen';

import {Button, Text, CartLineItem, CartEmpty} from '~/components';
import { useEffect } from 'react';

export function CartDetails({
  layout,
  onClose,
}: {
  layout: 'drawer' | 'page';
  onClose?: () => void;
}) {
  const {lines} = useCart();
  const scrollRef = useRef(null);
  const {y} = useScroll(scrollRef);

  if (lines.length === 0) {
    return <CartEmpty onClose={onClose} layout={layout} />;
  }

  const container = {
    drawer: 'grid grid-cols-1 h-screen-no-nav grid-rows-[1fr_auto]',
    page: 'pb-12 grid md:grid-cols-2 md:items-start gap-8 md:gap-8 lg:gap-12',
  };

  const content = {
    drawer: 'px-6 pb-6 sm-max:pt-2 overflow-auto transition md:px-12',
    page: 'flex-grow md:translate-y-4',
  };

  const summary = {
    drawer: 'grid gap-6 p-6 border-t md:px-12',
    page: 'sticky top-nav grid gap-6 p-4 md:px-6 md:translate-y-4 bg-primary/5 rounded w-full',
  };

  return (
    <form className={container[layout]}>
      <section
        ref={scrollRef}
        aria-labelledby="cart-contents"
        className={`${content[layout]} ${y > 0 ? 'border-t' : ''}`}
      >
        <ul className="grid gap-6 md:gap-10">
          {lines.map((line) => {
            return (
              <CartLineProvider key={line.id} line={line}>
                <CartLineItem />
              </CartLineProvider>
            );
          })}
        </ul>
      </section>
      <section aria-labelledby="summary-heading" className={summary[layout]}>
        <h2 id="summary-heading" className="sr-only">
          Order summary
        </h2>
        <OrderSummary />
        <CartCheckoutActions />
      </section>
    </form>
  );
}

function CartCheckoutActions() {
  const {lines} = useCart();
  const [isContado, setIsContado] = React.useState(false);

  useEffect(() => {
    console.log(isContado);
  }, [isContado]);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log('submit');
    console.log(isContado);
    console.log(lines);

    fetch('https://mirall-prod.wicode.com.mx/api/buy', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        contado: isContado,
        lines,
      }),
    }).then((res) => {
      return res.json();
      //window.location.href = res.url;
    }).then((data) => {
      window.location.href = data.url;
    }).catch((err) => {
      console.log(err);
    })

  }

  return (
    <>
      <div className="grid gap-4">
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: '0.5rem',
        }}>
          <input type="checkbox" id="cbox1" value="contado" checked={isContado} onChange={(e: any) => setIsContado(e.target.checked)} /> <p style={{ margin: 0 }}>Contado</p>
        </div>
        <button onClick={handleSubmit} className='btnVerde btnApp' >Comprar</button>
      </div>
    </>
  );
}

function OrderSummary() {
  const {cost} = useCart();
  return (
    <>
      <dl className="grid">
        <div className="flex items-center justify-between font-medium">
          <Text as="dt">Subtotal</Text>
          <Text as="dd">
            {cost?.subtotalAmount?.amount ? (
              <Money data={cost?.subtotalAmount} />
            ) : (
              '-'
            )}
          </Text>
        </div>
      </dl>
    </>
  );
}

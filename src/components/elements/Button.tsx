import clsx from 'clsx';
import {Link} from '@shopify/hydrogen';

import {missingClass} from '~/lib/utils';

export function  Button({
  as = 'button',
  className,
  variant = 'primary',
  width = 'auto',
  ...props
}: {
  as?: React.ElementType;
  className?: string;
  variant?: 'primary' | 'secondary' | 'inline';
  width?: 'auto' | 'full';
  [key: string]: any;
}) {
  const Component = props?.to ? Link : as;

  const baseButtonClasses =
    'inline-block font-medium text-center btnApp px-6F';

  const variants = {
    primary: `${baseButtonClasses}  text-contrast`,
    secondary: `${baseButtonClasses} bg-contrast txtBlanco`,
    inline: 'leading-none',
  };

  const widths = {
    auto: 'w-auto',
    full: 'w-full',
  };

  const styles = clsx(
    missingClass(className, 'bg-') && variants[variant],
    missingClass(className, 'w-') && widths[width],
    className,
  );

  return <Component className={styles} {...props} />;
}

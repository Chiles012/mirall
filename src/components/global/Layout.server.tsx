import { Suspense, useEffect, useState } from 'react';
import { useLocalization, useShopQuery, CacheLong, gql } from '@shopify/hydrogen';
import type { Menu, Shop } from '@shopify/hydrogen/storefront-api-types';

import { Header } from '~/components';
import { Footer } from '~/components/index.server';
import { parseMenu } from '~/lib/utils';

const HEADER_MENU_HANDLE = 'main-menu';
const FOOTER_MENU_HANDLE = 'footer';

const SHOP_NAME_FALLBACK = 'Hydrogen';

// const [location, setlLocation] = useState(window.location.pathname);

// useEffect(() => {
//   console.log(location);


//   return;
// }, [])


/**
 * A server component that defines a structure and organization of a page that can be used in different parts of the Hydrogen app
 */
export function Layout({ children }: { children: React.ReactNode }) {

  return (
    <>
      <div className="page flex flex-col min-h-screen">
        <div className="">
          <a href="#mainContent" className="sr-only">
            Skip to content
          </a>
        </div>
        <Suspense fallback={<Header title={SHOP_NAME_FALLBACK} />}>
          <HeaderWithMenu />
        </Suspense>
        <main role="main" id="mainContent" className="flex-grow">
          {children}

          {/* -------------------------- */}
          {/* {window.location.pathname === '/'} */}
          <div className="row formContacto justify-content-center">
            <div className="col-md-7 col-11">
            <h1>Contacto</h1>
              <div className="row">
                <div className="col input-group mb-3">
                  <span className="input-group-text" id="nombre">
                    <span className="material-symbols-outlined">
                      person
                    </span>
                  </span>
                  <input type="text" className="form-control" placeholder="Nombre" aria-label="Nombre" aria-describedby="basic-addon1" />
                </div>
                <div className="col input-group mb-3">
                  <span className="input-group-text" id="correo">
                    <span className="material-symbols-outlined">
                      mail
                    </span>
                  </span>
                  <input type="mail" className="form-control" placeholder="Corréo" aria-label="Corréo" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group mb-3">
                  <span className="input-group-text" id="numero">
                    <span className="material-symbols-outlined">
                      call
                    </span>
                  </span>
                  <input type="tel" className="form-control" placeholder="Numero de teléfono" aria-label="Numero de teléfono" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group">
                  <span className="input-group-text">
                    <span className="material-symbols-outlined">
                      comment
                    </span>
                  </span>
                  <textarea className="form-control" placeholder='Comentarios' aria-label="With textarea"></textarea>
                </div>
                <div />
              </div>
              <button className='col-3 btnEnviarCorreo'>Enviar</button>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-11">
              <h2 className=" grid w-full gap-8 p-6 py-8 md:p-8 lg:p-12 justify-items-start">
                Mayor información
              </h2>
              <div className='row wrapInfo'>
                <div className="multicolumn-card__info col-5">
                  <h3>Teléfono</h3>
                  <div className="rte">
                    <p>477 259 00 84</p>
                  </div>
                </div>
                <div className="multicolumn-card__info col-5">
                  <h3>Correo</h3>
                  <div className="rte">
                    <p>ventas@mirallstudio.com.mx</p>
                  </div>
                </div>
                <div className="multicolumn-card__info col-5">
                  <h3>San Miguel de Allende</h3>
                  <div className="rte">
                    <p>Calle Lupita No.2 Local 14 </p>
                    <p>Zona Centro (Centro Comercial Distrito Zona)</p>
                    <p>Guanajuato</p>
                  </div>
                </div>
                <div className="multicolumn-card__info col-5">
                  <h3>Puerto Vallarta</h3>
                  <div className="rte">
                    <p>Carrereta a Las Palmas No. 5460 Local 6</p>
                    <p>Las Juntas, Pto. Vallarta, Jalisco</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* -------------------------- */}
        </main>
      </div>
      <Suspense fallback={<Footer />}>
        <hr className='hrFooter' />
        <FooterWithMenu />
      </Suspense>
    </>
  );
}

function HeaderWithMenu() {
  const { shopName, headerMenu } = useLayoutQuery();
  return <Header title={shopName} menu={headerMenu} />;
}

function FooterWithMenu() {
  const { footerMenu } = useLayoutQuery();
  return <Footer menu={footerMenu} />;
}

function useLayoutQuery() {
  const {
    language: { isoCode: languageCode },
  } = useLocalization();

  const { data } = useShopQuery<{
    shop: Shop;
    headerMenu: Menu;
    footerMenu: Menu;
  }>({
    query: SHOP_QUERY,
    variables: {
      language: languageCode,
      headerMenuHandle: HEADER_MENU_HANDLE,
      footerMenuHandle: FOOTER_MENU_HANDLE,
    },
    cache: CacheLong(),
    preload: '*',
  });

  const shopName = data ? data.shop?.name : SHOP_NAME_FALLBACK;
  const customPrefixes = { BLOG: '', CATALOG: 'products' };

  const headerMenu = data?.headerMenu
    ? parseMenu(data.headerMenu, customPrefixes)
    : undefined;

  const footerMenu = data?.footerMenu
    ? parseMenu(data.footerMenu, customPrefixes)
    : undefined;

  return { footerMenu, headerMenu, shopName };
}

const SHOP_QUERY = gql`
  fragment MenuItem on MenuItem {
    id
    resourceId
    tags
    title
    type
    url
  }
  query layoutMenus(
    $language: LanguageCode
    $headerMenuHandle: String!
    $footerMenuHandle: String!
  ) @inContext(language: $language) {
    shop {
      name
    }
    headerMenu: menu(handle: $headerMenuHandle) {
      id
      items {
        ...MenuItem
        items {
          ...MenuItem
        }
      }
    }
    footerMenu: menu(handle: $footerMenuHandle) {
      id
      items {
        ...MenuItem
        items {
          ...MenuItem
        }
      }
    }
  }
`;
